package chessmen;

import java.util.ArrayList;

public class Bishop extends Piece{

	public Bishop(int color, Pos position) {
		super(color, position);
	}

	@Override
	public ArrayList<Pos> possibleMoves(Board b) {
		
		ArrayList<Pos> ret = moveDiagonally(b,this.color,this.pos) ; 
		return ret ; 
	}
	
	@Override
	protected boolean isLegalMove(Pos newPosition) {
		return true;
	}

	
	

}
