package chessmen;

import java.util.ArrayList;

public class King extends Piece {

	public King(int color, Pos position) {
		super(color, position);

		//this.points = 100; // potentially infinite
	}

	@Override
	public ArrayList<Pos> possibleMoves(Board b) {
      	ArrayList<Pos> ret = new ArrayList<>() ; 	
      	int[] incX = { -1, -1, -1, 0, 0, 1, 1, 1} ; 
		int[] incY = { -1, 0, 1, -1, 1, -1, 0,1} ; 
		for(int i = 0 ; i < 9 ; i++) {
			ret.addAll(move(b,this.color, this.pos.getX()+incX[i], this.pos.getY()+incY[i])) ; 
		}
		return ret ; 
	}
	 
	public ArrayList<Pos> move(Board b, int color, int currX, int currY) {
			ArrayList<Pos> ret = new ArrayList<>() ; 
			int i = currX;
			int j = currY ;
				if (b.board[i][j].isEmpty()) {
					ret.add(new Pos(i,j)) ; 
				}
				else if (this.hasSameColorPiece(b,i,j)) {
				}
				else if (this.hasOpponentPiece(b,i,j)) {
					ret.add(new Pos(i,j)) ; 
				}
			return ret ; 
		}

	@Override
	protected boolean isLegalMove(Pos newPosition) {
		return true ; 
	}

}
