
package chessmen;

import java.util.ArrayList;

public class Knight extends Chessman {

    public Knight(int color, Pos pos) {
        super(color, pos);
    }

    @Override
    public ArrayList<Pos> possibleMoves(Board b) {
    

      	ArrayList<Pos> ret = new ArrayList<>() ; 	
      	
      	int[] incX = { -2, -2, -1, 1, 2, 2, 1, -1} ; 
		int[] incY = { -1, 1, 2, 2, 1, -1, -2, -2} ; 
		
		for(int i = 0 ; i < 8 ; i++) {
			ret.addAll(move(b,this.color, this.pos.getX()+incX[i], this.pos.getY()+incY[i])) ; 
		}
		
		return ret ; 
    }
    
    public ArrayList<Pos> move(Board b, int color, int X, int Y) {
		
    		ArrayList<Pos> ret = new ArrayList<>() ; 
		
    		int i = X;
		int j = Y ;
		
		if (b.board[i][j].isEmpty()) {
			ret.add(new Pos(i,j)) ; 
		}
		
		else if (this.hasOpponentPiece(b,i,j)) {
			ret.add(new Pos(i,j)) ; 
		}
		
		return ret ; 
	}
	

	@Override
	protected boolean isLegalMove(Pos newPosition) {
		
		if((newPosition.getX() == this.pos.getX() - 2 &&  newPosition.getY() == this.pos.getY() - 1 || newPosition.getY() == this.pos.getY() + 1 ) 
		|| (newPosition.getX() == this.pos.getX() + 2 &&  newPosition.getY() == this.pos.getY() - 1 || newPosition.getY() == this.pos.getY() + 1 ) 
		/* 2 more conditions */)
		{
			return true;
		}
	
		return false;
	}


}