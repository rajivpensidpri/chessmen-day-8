package chessmen;

import java.util.ArrayList;
import java.util.Collection;

public class Pawn extends Chessman {
	
	//TODO : Update this variable in the Pawn's Move function when it is written. 
	public boolean hasMovedOnce ;   
	
	public static final int UP = -1 ; 
	public static final int DOWN = 1 ; 
	
	 
	
    public Pawn(int color, Pos position, boolean isAlive){
        super(color,position);
        this.hasMovedOnce = false ;
    }

    @Override
    protected boolean isLegalMove(Pos position) {
        return false;
    }
    
  //Assuming Black Pawns will be down the screen and White Pawns at the top of the screen.
  // Also the board is a matrix that starts in topLeft(0,0) and ends in bottomRight(7,7). 
    public int setDirectionForPawn() {
    		if (this.color == BLACK) {
    			return DOWN ; 
    		}
    		return UP ; 
    }

	public ArrayList<Pos> possibleMoves(Board b) {
		
		int direction = this.setDirectionForPawn() ; 
		
		return pawnMoves(b,this.pos.getX(),this.pos.getY(),direction) ; 
	}



	private ArrayList<Pos> pawnMoves(Board b, int x, int y, int direction) {
		ArrayList<Pos> ret = new ArrayList<>() ; 
		
		Chessman ch = b.board[x][y+direction] ; 
		
		if (ch.isEmpty() && isValid(x,y+direction)) {
			ret.add(new Pos(x,y+direction)) ; 
		}
		
		ch = b.board[x][y+2*direction] ; 
		
		if (this.hasMovedOnce == false && ch.isEmpty()) {
			ret.add(new Pos(x,y+2*direction)) ; 
		}
		
		if (this.hasOpponentPiece(b,x+1,y+direction)) {
			ret.add(new Pos(x+1,y+direction)) ; 
		}
		
		if (this.hasOpponentPiece(b,x-1,y+direction)) {
			ret.add(new Pos(x-1,y+direction)) ; 
		}
		
		return ret ;
	}

}
