package chessmen;

public class Pos {
	private int x ; 
	private int y ; 
	
	public Pos(int i, int j) {
		this.x = i ; 
		this.y = j ; 
	}
	public int getX() {
		return this.x ; 
	}
	public int getY() {
		return this.y ; 
	}
	public void setX(int x){
		this.x = x;
	}
	public void setY(int y){
		this.y = y;
	}
	
}
