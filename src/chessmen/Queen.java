package chessmen;

import java.util.ArrayList;

public class Queen extends Piece{
    
	public Queen(int color, Pos position, boolean isAlive){
        super(color,position);
    }

    @Override
    public ArrayList<Pos> possibleMoves(Board b) {
       
    	   ArrayList<Pos> ret = new ArrayList<>() ; 
       
       Bishop bish = new Bishop(this.color, this.pos) ; 
       Rook rook = new Rook(this.color, this.pos) ; 
       
       ret.addAll(bish.possibleMoves(b)) ; 
       ret.addAll(rook.possibleMoves(b)) ; 
       
       return ret ; 
    }

    @Override
    protected boolean isLegalMove(Pos position) {
       return true ; 
    }

}
