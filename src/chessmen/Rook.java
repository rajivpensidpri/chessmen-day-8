package chessmen;

import java.util.ArrayList;

public class Rook extends Piece {

	public Rook(int color, Pos position) {
		super(color, position);
	}

	@Override
	public ArrayList<Pos> possibleMoves(Board b) {
		
		return  moveStraight(b,this.color,this.pos) ; 
		 
	}

	@Override
	protected boolean isLegalMove(Pos position) {
		return true ;
	}
	
	
}
