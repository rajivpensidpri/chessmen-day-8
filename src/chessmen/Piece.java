package chessmen;

import java.util.ArrayList;
import java.util.Collection;

public abstract class Piece extends Chessman{

	public Piece(int color, Pos position) {
		super(color, position);
	}

	public ArrayList<Pos> possibleMoves(Board b) {
		return new ArrayList<>() ;
	}
	
	public ArrayList<Pos> moves(Board b, int color, int currX, int currY, int incx, int incy) {
		ArrayList<Pos> ret = new ArrayList<>() ; 
		
		Chessman curr = b.board[currX][currY] ; 
		
		for(int i = currX+incx, j = currY+incy ; isValid(i,j) ; i += incx, j += incy ) {
			
			if (b.board[i][j].isEmpty()) {
				ret.add(new Pos(i,j)) ; 
			}
			
			else if (curr.hasSameColorPiece(b,i,j)) {
				break ; 
			}
			
			else if (curr.hasOpponentPiece(b,i,j)) {
				ret.add(new Pos(i,j)) ; 
				break ; 
			}
		}
		
		return ret ; 
	}
	

	protected  ArrayList<Pos> moveDiagonally(Board b, int color,Pos pos) {
		
		ArrayList<Pos> ret = new ArrayList<>() ; 
		
		int[] incX = { 1, 1, -1, -1 } ; 
		int[] incY = { 1, -1, 1, -1 } ; 
		
		for(int i = 0 ; i < 4 ; i++) {
			ret.addAll(moves(b,color, pos.getX(), pos.getY()+incY[i], incX[i],incY[i])) ; 
		}
		
		return ret ; 
	}
	
	
	protected  ArrayList<Pos> moveStraight(Board b, int color,Pos pos) {
		
		ArrayList<Pos> ret = new ArrayList<>() ; 
		
		int[] incX = {1, -1, 0, 0 } ; 
		int[] incY = {0,  0, 1, -1} ;  
		
		for(int i = 0 ; i < 4 ; i++) {
			ret.addAll(moves(b,color,pos.getX(), pos.getY(),incX[i],incY[i])); 
		}
	
		return ret ; 
	}
	
}
