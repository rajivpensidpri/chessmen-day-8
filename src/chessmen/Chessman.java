package chessmen;

import java.util.ArrayList;

public abstract class Chessman {
	public Pos pos ; 
	public boolean isAlive ; 
	public int color ; 
	
	public static final int NOMAN = 0 ; 
	public static final int WHITE = 1 ; 
	public static final int BLACK = 2 ; 
	
	
	
	public Chessman(int color, Pos position){
		this.color = color;
		this.isAlive = true;
		this.pos = position;
	}

	public abstract ArrayList<Pos> possibleMoves(Board board);
	protected abstract boolean isLegalMove(Pos newPos);


	public void setIsAlive(boolean isAlive) {
		this.isAlive = isAlive;
	}

	public boolean isAlive() {
		return this.isAlive;
	}

	public boolean isBlack() {
		return (this.color == BLACK) ;
	}

	public boolean isWhite() {
		return (this.color == WHITE) ;
	}
	public boolean notAChessMan() {
		return (this.color == NOMAN) ; 
	}
	
	protected boolean isEmpty() {
		 return ( this.color == NOMAN && this.isValid(pos.getX(), pos.getY())) ; 
		
	}

	public boolean hasSameColorPiece(Board b, int i, int j) {
		if (this.color == b.board[i][j].color && isValid(i,j))
			return true ;
		return false ; 
	}

	public boolean hasOpponentPiece(Board b, int i, int j) {
		if (this.color != b.board[i][j].color && isValid(i,j))
			return true ;
		return false ; 
	}
	public boolean isValid(int x, int y) {
		
		if(x>=0 && x <=7 && y>=0 && y<=7) {
			return true ;
		}
		return false;
	}
	
	
}
