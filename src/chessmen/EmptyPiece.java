package chessmen;

import java.util.ArrayList;

public class EmptyPiece extends Chessman {

	public EmptyPiece(Pos pos) {   // Presence of this chessman means that there is no chessman at that pos of board. Chill, its just an abstraction.  
		super(0, pos) ; 
		this.pos = pos ; 
	}
	
	@Override
	public ArrayList<Pos> possibleMoves(Board board) {
		// TODO Auto-generated method stub
		return new ArrayList<>();
	}

	@Override
	protected boolean isLegalMove(Pos newPos) {
		// TODO Auto-generated method stub
		return false;
	}

}
